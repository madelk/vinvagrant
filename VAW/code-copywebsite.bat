@echo off
if %1.==. (
    echo You need to pass in a websiteid "code-copywebsite 3050"
	GOTO eof
) else (
	mkdir c:\codeforvagrant\vaw\DealerSites
	robocopy \\dealersitedev\dealersites_content\%1 c:\codeforvagrant\vaw\DealerSites\%1 /MIR /R:5 /W:5 /XD dealersites
)

:eof