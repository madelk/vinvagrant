@echo off

if exist dotnet.exe (
    rem file exists
) else (
	echo Downloading dotnet
    @powershell -Command "(new-object net.webclient).DownloadFile('http://download.microsoft.com/download/E/2/1/E21644B5-2DF2-47C2-91BD-63C560427900/NDP452-KB2901907-x86-x64-AllOS-ENU.exe', 'dotnet.exe')"
)

vagrant up
explorer http://127.0.0.1:8020
pause