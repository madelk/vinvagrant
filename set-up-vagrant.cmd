@echo off
cls
echo Make sure you are running this as an admin, or this will go pear shaped.
echo If you are not an admin press CTRL+C and run again.
echo.
echo Otherwise press any key to set up the required peices to use Vagrant
echo.
echo.
pause
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin

C:\ProgramData\chocolatey\choco.exe install virtualbox.extensionpack --force -y
C:\ProgramData\chocolatey\choco.exe install vagrant --force -y



echo.
echo If you didn't just see a whole lot of red scroll by, everything should be okay.
echo You'll probably want to reboot though, things have a habbit of not getting into PATH
pause