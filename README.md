# README #

### What is this repository for? ###

* Create a way for VinSolutions staff without local servers to be able to run VinSolutions code with minimal fuss

### How do I get set up? ###

* Start an admin CMD window
* run "**set-up-vagrant**" this will install all the required software to run a VM
* Enter the CRM folder "cd CRM"
* run "**code-get**" or "**code-get ci_xxxxxx**" to download a build to your machine, or run "**code-seeavailable**" to see available builds (not all builds may be complete". This will download a completed CI build to your C: drive
* run "**server-start**" to configure and run a windows server
* You can run the code by browsing to [http://127.0.0.1:8020](http://127.0.0.1:8020)
* Once you are done, be sure to run "**server-stop**" to free up resources
* If you wish to free up drive space, or rebuild the server from scratch, run "**server-destroy**" you can also run "**code-destroy**" to clean all the code from your machine

### Contribution guidelines ###

* Please use the [Issue Tracker](https://bitbucket.org/madelk/vinvagrant/issues/) to log issues
* Feel free to make your own commits, probably... I don't know what I'd doing

### Who do I talk to? ###

* Created my [Mark Dell](mailto:mark.dell@vinsolutions.com)